package com.example.wouter.dictionary;


//https://bitbucket.org/wouter_baeyens/mobile-dictionary
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.wouter.dictionary.domain.SoapCall;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void onSelectDictionariesClicked(MenuItem item) {
        System.out.println("starting lookup");
        Intent i = new Intent(this, SelectDictionaryActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.my_menu, menu);

        return true;

    }


    public void onLookupClicked(View view) {
        Intent intent = getIntent();
        String ids = intent.getStringExtra("ids");
        System.out.println("Whooo: " + ids.toString());
    }
}
