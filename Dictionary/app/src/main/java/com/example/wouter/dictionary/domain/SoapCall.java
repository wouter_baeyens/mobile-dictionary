package com.example.wouter.dictionary.domain;

import android.os.AsyncTask;
import android.util.Log;

import com.example.wouter.dictionary.OnTaskCompleted;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import static android.provider.ContactsContract.CommonDataKinds.Identity.NAMESPACE;


/**
 * Created by Wouter on 27/01/2017.
 */

public class SoapCall extends AsyncTask<String, Integer, String> {


    public final static String OPERATION_NAME = "DictionaryList";
    public final static String NAMESPACE = "http://services.aonaware.com/webservices/";
    public final static String SOAP_ACTION = NAMESPACE + OPERATION_NAME;
    public final static String URL = "http://services.aonaware.com/DictService/DictService.asmx";

    private OnTaskCompleted listener;

    public SoapCall(OnTaskCompleted listener){
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params){
        String response = null;
        SoapObject Request = new SoapObject(NAMESPACE, OPERATION_NAME);
        //Request.addProperty("strCommand", params[0]);
        //Request.addProperty("strCommandParameters", params[1]);


        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        soapEnvelope.dotNet = true;
        soapEnvelope.setOutputSoapObject(Request);
        // Needed to make the internet call

        // Allow for debugging - needed to output the request

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;
        // this is the actual part that will call the webservice
        try {
            androidHttpTransport.call(SOAP_ACTION, soapEnvelope);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        SoapObject result = null;
        // Get the SoapResult from the envelope body.
        if (soapEnvelope.bodyIn instanceof SoapFault) {
            String str = ((SoapFault) soapEnvelope.bodyIn).faultstring;
            Log.i("", str);
        } else {
            result = (SoapObject) soapEnvelope.bodyIn;

            response = result.getProperty(0).toString();
            System.out.println("RESPONSE!: " + response);
        }
        listener.onTaskCompleted(result);
        return response;
    }


}
