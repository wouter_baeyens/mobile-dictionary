package com.example.wouter.dictionary.domain;

/**
 * Created by Wouter on 27/01/2017.
 */

public class Dictionary {

    public String id, name;

    public Dictionary(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String toString(){
        return id + ": " + name;
    }
}
