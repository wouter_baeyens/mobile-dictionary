package com.example.wouter.dictionary;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Wouter on 27/01/2017.
 */

public interface OnTaskCompleted {
    void onTaskCompleted(SoapObject response);
}
