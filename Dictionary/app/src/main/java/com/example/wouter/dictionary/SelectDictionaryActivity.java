package com.example.wouter.dictionary;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.wouter.dictionary.domain.Dictionary;
import com.example.wouter.dictionary.domain.SoapCall;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wouter on 27/01/2017.
 */

public class SelectDictionaryActivity extends AppCompatActivity implements OnTaskCompleted{

    ListView listView;
    DictionaryAdapter adapter;
    private List<String> dictionaryIds = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_dictionary);
        adapter = new DictionaryAdapter();
        listView =  (ListView) findViewById(R.id.dictionary_list);
        System.out.println("LIIISSTT: " + listView.toString());
        listView.setAdapter(adapter);
        SoapCall call = new SoapCall(this);
        call.execute();
    }

    public void Refresh(View view) {
        System.out.println(dictionaryIds.toString());
        setResult(0, getIntent().putExtra("ids", dictionaryIds.toString()));
        finish();

    }

    @Override
    public void onTaskCompleted(SoapObject response) {

        List<Dictionary> dictionaryList = new ArrayList();
        SoapObject dictionaries = (SoapObject) response.getProperty("DictionaryListResult");
        System.out.print(dictionaries.toString());
        int nr = dictionaries.getPropertyCount();
        for(int i = 0;i<nr;i++){
            SoapObject dict = (SoapObject) dictionaries.getProperty(i);
            System.out.println("-> " +  dict.toString());
            String id = dict.getPropertyAsString("Id");
            String name = dict.getPropertyAsString("Name");
            dictionaryList.add(new Dictionary(id, name));
        }
        adapter.dictionaryList = dictionaryList;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.update();
            }
        });

    }

    private class DictionaryAdapter extends BaseAdapter{
        private List<Dictionary> dictionaryList = new ArrayList<>();

        public DictionaryAdapter(){
            update();
        }

        @Override
        public int getCount() {
            return dictionaryList.size();
        }

        @Override
        public Object getItem(int i) {
            return dictionaryList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return Long.valueOf(dictionaryList.get(i).id).longValue();

        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            CheckBox viewButton = (CheckBox) view;
            if(viewButton == null)
                viewButton = createViewItem();
            viewButton.setText(dictionaryList.get(i).name);
            viewButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    String id = dictionaryList.get(i).id;
                    if(isChecked){
                        dictionaryIds.add(id);
                    } else{
                        dictionaryIds.remove(id);
                    }
                }

            });
            return viewButton;
        }

        public void update(){
            notifyDataSetChanged();
        }


    }

    private CheckBox createViewItem(){
        return new CheckBox(this);
    }
}
